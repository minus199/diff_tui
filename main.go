package main

import (
	"minus.com/diff_tui/cli"
	"minus.com/diff_tui/service/spy"
)

func main() {
	prefs := cli.ExtractUserPrefs()
	spy.Start(prefs.SrcDir, prefs.TargetDir)
}
