package cli

import (
	"github.com/jaffee/commandeer/cobrafy"
	"log"
)

type AppOpts struct {
	SrcDir    []string `help:"How many does it take?"`
	TargetDir []string `help:"What did they get?"`
}

func NewOpts() *AppOpts {
	return &AppOpts{
		SrcDir: []string{"./"},
	}
}

func (m *AppOpts) Run() error {
	return nil
}

func ExtractUserPrefs() *AppOpts {
	prefs := NewOpts()

	err := cobrafy.Execute(prefs)
	if err != nil {
		log.Fatal(err)
	}

	if len(prefs.TargetDir) == 0 {
		log.Fatal("Must specify target folder")
	}

	return prefs
}
