package utils

import (
	"os"
	"log"
)

func ValidateDir(path string) bool {
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		log.Fatalf("Directory %s does not exists", path)
	}

	if os.IsPermission(err) {
		log.Fatalf("Directory %s: permission denied", path)
	}

	return true
}

func Validate(path string, forWrite bool) *os.File {
	flags := os.O_RDWR | os.O_APPEND | os.O_CREATE
	f, err := os.OpenFile(path, flags, 0644)
	if err != nil {
		log.Fatal(err)
	}

	return f
}

func Copy(srcFile *os.File, targetFile *os.File) *os.File {
	srcStats, err := srcFile.Stat()
	targetData := make([]byte, srcStats.Size())

	if _, err := srcFile.Read(targetData); err != nil {
		log.Fatal(err)
	}
	if _, err := targetFile.Write(targetData); err != nil {
		log.Fatal(err)
	}

	err = targetFile.Sync()

	if err != nil {
		log.Fatal(err)
	}

	return targetFile
}

func LoadFile(fileName string) string {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}

	stats, err := file.Stat()
	data := make([]byte, stats.Size())

	count, err := file.Read(data)
	if err != nil {
		log.Fatal(err)
	}

	return string(data[:count])
}