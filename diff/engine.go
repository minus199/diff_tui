package diff

import (
	"github.com/sergi/go-diff/diffmatchpatch"
	"io/ioutil"
	"log"
	"strings"
	"math"
)

func extractContent(fileLeft, fileRight string) ([]string, []string) {
	contentBytesLeft, errLeft := ioutil.ReadFile(fileLeft)
	contentBytesRight, errRight := ioutil.ReadFile(fileRight)

	if errRight != nil || errLeft != nil {
		log.Fatal(errLeft, errRight)
	}

	srcContent := strings.Split(string(contentBytesLeft), "\n")
	targetContent := strings.Split(string(contentBytesRight), "\n")

	return prepareContainers(srcContent, targetContent)
}

func prepareContainers(srcContent, targetContent []string) ([]string, []string) {
	normLen := int(math.Max(float64(len(srcContent)), float64(len(targetContent))))
	containerSrc := make([]string, normLen)
	containerTarget := make([]string, normLen)
	for index := range containerSrc {
		if containerSrc[index] = ""; len(srcContent) > index {
			containerSrc[index] = srcContent[index]
		}

		if containerTarget[index] = ""; len(targetContent) > index {
			containerTarget[index] = targetContent[index]
		}
	}

	return containerSrc, containerTarget
}

// Compute Computes the diff and normalizes
func Compute(fileLeft, fileRight string) *Line {
	srcContent, targetContent := extractContent(fileLeft, fileRight)
	activeLine := NewLine()
	topLine := activeLine

	dmp := diffmatchpatch.New()
	for lineNum, rawSrcLine := range srcContent {
		rawTargetLine := targetContent[lineNum]
		currentDiffs := dmp.DiffMain(rawTargetLine, rawSrcLine, false)
		activeLine = activeLine.Chain(currentDiffs)
	}

	return topLine
}
