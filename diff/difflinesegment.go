package diff

import (
	dmp "github.com/sergi/go-diff/diffmatchpatch"
	"github.com/marcusolsson/tui-go"
	"fmt"
)

type Segment struct {
	content     string
	diffType    dmp.Operation
	position    int
	label       *tui.Label
	isHighlight bool
}

// ComputeStyle
func (segment *Segment) computeStyle() string {
	switch segment.diffType {
	case dmp.DiffInsert:
		return "add"
	case dmp.DiffDelete:
		return "delete"
	case dmp.DiffEqual:
		return "eq"
	}

	return "eq"
}

func (segment *Segment) SetStyle(highlight bool) *Segment {
	if highlight == true {
		segment.boundLabel().SetStyleName("highlight-" + segment.computeStyle())
	} else {
		segment.boundLabel().SetStyleName(segment.computeStyle())
	}

	segment.isHighlight = highlight
	return segment
}

func (segment *Segment) BindLabel(label *tui.Label) *Segment {
	segment.label = label
	return segment
}

func (segment *Segment) boundLabel() *tui.Label {
	return segment.label
}

func (segment Segment) String() string {
	return fmt.Sprintf("%s", segment.content)
}
