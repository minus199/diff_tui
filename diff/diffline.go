package diff

import (
	dmp "github.com/sergi/go-diff/diffmatchpatch"
	"fmt"
)

type Line struct {
	lineNum      int
	segments     []*Segment
	previousLine *Line
	nextLine     *Line
}

func NewLine() (*Line) {
	return &Line{lineNum: 1}
}

func (line *Line) Chain(currentDiffs []dmp.Diff) *Line {
	for index, diff := range currentDiffs {
		line.AddSegment(index, diff)
	}

	nextLine := &Line{lineNum: line.lineNum + 1, previousLine: line}
	line.nextLine = nextLine
	return nextLine
}

func (line *Line) LineNum() int {
	return line.lineNum
}

func (line *Line) AddSegment(pos int, diff dmp.Diff) {
	segment := &Segment{content: diff.Text, diffType: diff.Type, position: pos}
	line.segments = append(line.segments, segment)
}

func (line *Line) Segments() []*Segment {
	return line.segments
}

//2.075
func (line *Line) Prev() *Line {
	return line.previousLine
}

func (line *Line) Next() *Line {
	return line.nextLine
}

//Rewind traverse till top line
func (line *Line) Rewind() *Line {
	fmt.Println(fmt.Sprintf("line %v", line.lineNum))
	x := line.previousLine
	if x != nil {
		return x.Rewind()
	}

	return line
}

//Forward traverse till bottom line
func (line *Line) Forward(f ...func(*Line)) *Line {
	for _, cbFunc := range f {
		if cbFunc != nil{
			cbFunc(line)
		}
	}

	if line.nextLine != nil {
		return line.nextLine.Forward(f...)
	}

	return line
}
