package spy

import (
	"log"
	"github.com/fsnotify/fsnotify"
	"path/filepath"
	"minus.com/diff_tui/diff"
	"os"
	"os/signal"
	"syscall"
	"fmt"
	"minus.com/diff_tui/console"
)

func Start(srcDirs []string, targetDirs []string) {
	srcs := NewSourceDirectory(targetDirs, srcDirs...)
	watcher := newWatcher()
	defer watcher.Close()

	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println(fmt.Sprintf("Exiting %v", sig))
		done <- true
	}()

	go watch(watcher, srcs)
	for _, dir := range srcs {
		addDir(watcher, *dir)
	}

	<-done
}

func newWatcher() *fsnotify.Watcher {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	return watcher
}

func watch(watcher *fsnotify.Watcher, srcs map[string]*Directory) {
	for {
		select {
		case event := <-watcher.Events:
			currentSrc := srcs[filepath.Dir(event.Name)]
			srcFile, targetFiles := currentSrc.BoundFile(event.Name)
			for _, currentTarget := range targetFiles {
				topLine := diff.Compute(srcFile, currentTarget)
				//fmt.Println(topLine)
				//console.Prompt(fmt.Sprintf("Got changes on %v. Show? ", srcFile), console.NilOrY)
				console.Make(topLine).Run()
				//if console.Prompt(fmt.Sprintf("Closed %v. Should commit? ", srcFile), console.NilOrY) {
				//	console.Prompt(fmt.Sprintf("Commited to %v", srcFile), console.NilOrY)
				//}
			}
		case err := <-watcher.Errors:
			log.Println("error:", err)
		}
	}
}

func addDir(watcher *fsnotify.Watcher, dir Directory) {
	err := watcher.Add(dir.Path())
	if err != nil {
		log.Fatal(err)
	}
}
