package spy

import (
	"minus.com/diff_tui/utils"
	"os"
	"path/filepath"
	"log"
)

func newDirectory(dir string, dirType DirectoryType) *Directory {
	utils.ValidateDir(dir)

	workingDir, _ := os.Getwd()
	relPath, err := filepath.Rel(workingDir, dir)
	if err != nil {
		log.Fatal()
	}

	absPath, err := filepath.Abs(dir)
	if err != nil {
		log.Fatal()
	}

	return &Directory{dirType: dirType, relPath: relPath, absPath: absPath}
}

func newDirectories(dirType DirectoryType, dirs ...string) map[string]*Directory {
	//targets := make([]*Directory, len(dirs))
	targets := make(map[string]*Directory)

	for _, dir := range dirs {
		targets[dir] = newDirectory(dir, dirType)
	}

	return targets
}

func NewSourceDirectory(targetDirs []string, dirs ...string) map[string]*Directory {
	targets := newDirectories(DirTypeTarget, targetDirs...)
	srcs := newDirectories(DirTypeSrc, dirs...)

	for _, srcDir := range srcs {
		srcDir.boundDirectory = targets
	}

	return srcs
}
