package spy

import (
	"github.com/jessevdk/go-flags"
	"fmt"
	"minus.com/diff_tui/utils"
	"os"
	"path/filepath"
)

type DirectoryType int

const (
	DirTypeSrc    DirectoryType = iota
	DirTypeTarget
)

type IDirectory interface {
	flags.Marshaler
	flags.Unmarshaler

	Path() string
}

type Directory struct {
	relPath        string
	absPath        string
	boundDirectory map[string]*Directory
	dirType        DirectoryType
}

func (directoryDescriptor Directory) MarshalFlag() (string, error) {
	return fmt.Sprintf("%s, %s", directoryDescriptor.absPath, directoryDescriptor.relPath), nil
}

func (directoryDescriptor *Directory) UnmarshalFlag(value string) error {
	utils.ValidateDir(value)
	workingDir, _ := os.Getwd()
	directoryDescriptor.relPath, _ = filepath.Rel(workingDir, value)
	directoryDescriptor.absPath, _ = filepath.Abs(value)

	return nil
}

func (directoryDescriptor *Directory) Path() string {
	return directoryDescriptor.absPath
}

func (directoryDescriptor *Directory) Validate() *Directory {
	utils.ValidateDir(directoryDescriptor.absPath)
	return directoryDescriptor
}

// BoundFile returns the corresponding file in all target directories
func (directoryDescriptor *Directory) BoundFile(path string) (string, []string) {
	if directoryDescriptor.dirType == DirTypeTarget {
		return filepath.Join(directoryDescriptor.absPath, path), nil
	}

	c := 0
	srcFile := path
	output := make([]string, len(directoryDescriptor.boundDirectory))
	for _, bound := range directoryDescriptor.boundDirectory {
		targetFile, _ := bound.BoundFile(filepath.Base(path))
		output[c] = targetFile
		c++
	}

	return srcFile, output
}
