package tuigo

import (
	"fmt"
	"log"
	"time"

	"github.com/marcusolsson/tui-go"
)

type post struct {
	username string
	message  string
	time     string
}

var posts = []post{
	{username: "john", message: "hi, what's up?", time: "14:41"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
	{username: "jane", message: "not much", time: "14:43"},
}

func C() {
	sidebar := tui.NewVBox(
		tui.NewLabel("CHANNELS"),
		tui.NewLabel("general"),
		tui.NewLabel("random"),
		tui.NewLabel(""),
		tui.NewLabel("DIRECT MESSAGES"),
		tui.NewLabel("slackbot"),
		tui.NewSpacer(),
	)
	sidebar.SetBorder(true)

	history := tui.NewVBox()

	for _, m := range posts {
		history.Append(tui.NewHBox(
			tui.NewLabel(m.time),
			tui.NewPadder(1, 0, tui.NewLabel(fmt.Sprintf("<%s>", m.username))),
			tui.NewLabel(m.message),
			tui.NewSpacer(),
		))
	}

	historyScroll := tui.NewScrollArea(history)
	// historyScroll.SetAutoscrollToBottom(true)
	// historyScroll.SetFocused(true)

	historyBox := tui.NewVBox(historyScroll)
	historyBox.SetBorder(true)

	input := tui.NewEntry()
	input.SetFocused(true)
	input.SetSizePolicy(tui.Expanding, tui.Maximum)

	inputBox := tui.NewHBox(input)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	chat := tui.NewVBox(historyBox, inputBox)
	chat.SetSizePolicy(tui.Expanding, tui.Expanding)

	input.OnSubmit(func(e *tui.Entry) {
		history.Append(tui.NewHBox(
			tui.NewLabel(time.Now().Format("15:04")),
			tui.NewPadder(1, 0, tui.NewLabel(fmt.Sprintf("<%s>", "john"))),
			tui.NewLabel(e.Text()),
			tui.NewSpacer(),
		))
		input.SetText("")
		historyScroll.ScrollToBottom()
	})

	root := tui.NewHBox(sidebar, chat)

	ui, err := tui.New(root)
	if err != nil {
		log.Fatal(err)
	}

	ui.SetKeybinding("Esc", func() { ui.Quit() })
	ui.SetKeybinding("q", func() { ui.Quit() })

	ui.SetKeybinding("Up", func() {
		historyScroll.Scroll(0, -1)
	})
	ui.SetKeybinding("Down", func() {
		historyScroll.Scroll(0, -1)
	})
	ui.SetKeybinding("Left", func() {
		historyScroll.Scroll(0, -1)
	})
	ui.SetKeybinding("Right", func() {
		historyScroll.Scroll(0, -1)
	})

	ui.SetKeybinding("a", func() { historyScroll.SetAutoscrollToBottom(true) })
	ui.SetKeybinding("t", func() { historyScroll.ScrollToTop() })
	ui.SetKeybinding("b", func() { historyScroll.ScrollToBottom() })

	if err := ui.Run(); err != nil {
		log.Fatal(err)
	}
}
