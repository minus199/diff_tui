package console

import (
	"os/exec"
)

func Start() {
	cmd := exec.Command("./bin/DiffChief")
	grepOut, _ := cmd.StdoutPipe()
	cmd.Start()
	grepOut.Read(make([]byte, 100))
}
