package console

import (
	"image"
	"github.com/marcusolsson/tui-go"
)

type ScrollAreaX struct {
	tui.WidgetBase

	Widget     tui.Widget
	topLeft    image.Point
	autoscroll bool
}

// Newtui.ScrollArea returns a new tui.ScrollArea.
func NewScrollAreaX(w tui.Widget) *ScrollAreaX {
	return &ScrollAreaX{
		Widget: w,
	}
}

// MinSizeHint returns the minimum size the widget is allowed to be.
func (s *ScrollAreaX) MinSizeHint() image.Point {
	return s.MinSizeHint()
}

// SizeHint returns the size hint of the underlying widget.
func (s *ScrollAreaX) SizeHint() image.Point {
	return image.Pt(15, 8)
}

// Scroll shifts the views over the content.
func (s *ScrollAreaX) Scroll(dx, dy int) {
	s.topLeft.X += dx
	s.topLeft.Y += dy
}

// ScrollToBottom ensures the bottom-most part of the scroll area is visible.
func (s *ScrollAreaX) ScrollToBottom() {
	s.topLeft.Y = s.Widget.SizeHint().Y - s.Size().Y
}

// ScrollToTop resets the vertical scroll position.
func (s *ScrollAreaX) ScrollToTop() {
	s.topLeft.Y = 0
}

// SetAutoscrollToBottom makes sure the content is scrolled to bottom on resize.
func (s *ScrollAreaX) SetAutoscrollToBottom(autoscroll bool) {
	s.autoscroll = autoscroll
}

// Draw draws the scroll area.
func (s *ScrollAreaX) Draw(p *tui.Painter) {
	p.Translate(-s.topLeft.X, -s.topLeft.Y)
	defer p.Restore()

	off := image.Point{s.topLeft.X, s.topLeft.Y}
	p.WithMask(image.Rectangle{Min: off, Max: s.Size().Add(off)}, func(p *tui.Painter) {
		s.Widget.Draw(p)
	})
}

// Resize resizes the scroll area and the underlying widget.
func (s *ScrollAreaX) Resize(size image.Point) {
	s.Widget.Resize(s.Widget.SizeHint())
	s.WidgetBase.Resize(size)

	if s.autoscroll {
		s.ScrollToBottom()
	}
}
