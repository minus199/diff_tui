package console

import (
	"log"

	"github.com/marcusolsson/tui-go"
	"minus.com/diff_tui/diff"
	"os"
	"sync"
	"fmt"
)

// DiffTUI ..
type DiffTUI struct {
	ui          tui.UI
	initError   error
	theme       *tui.Theme
	content     *DiffContent
	prompt      *PromptScreen
	statusLabel *tui.Label
	root        *tui.Grid
}

var instance *DiffTUI

var once sync.Once

func getInstance(topLine *diff.Line) *DiffTUI {
	once.Do(func() {
		instance = &DiffTUI{}
		instance.init(topLine)
	})

	return instance
}

// Make a new DiffTUI
func Make(topLine *diff.Line) *DiffTUI {
	instance = getInstance(topLine).injectDiff()
	return instance
}

// Init ...
func (dtui *DiffTUI) init(topLine *diff.Line) *DiffTUI {
	dtui.content = NewDiffContent(topLine, dtui)
	dtui.prompt = NewPromptDefaultAction(dtui)
	dtui.statusLabel = tui.NewLabel("")
	dtui.statusLabel.SetWordWrap(true)
	dtui.statusLabel.SetSizePolicy(tui.Minimum, tui.Minimum)

	dtui.root = tui.NewGrid(2, 0)
	dtui.root.AppendRow(dtui.statusLabel, dtui.prompt.root)
	dtui.root.AppendRow(dtui.statusLabel, dtui.content.root)

	ui, err := tui.New(dtui.root)
	dtui.ui = ui
	dtui.initError = err

	return dtui
}

func (dtui *DiffTUI) injectDiff() *DiffTUI {
	dtui.content.container.RemoveRows()

	dtui.content.topLine.Forward(func(line *diff.Line) {
		dtui.content.appendDiffLine(line)
	})

	return dtui
}

// Run ..
func (dtui *DiffTUI) Run() *DiffTUI {
	dtui.validate().setTheme().shutdownHooks().keyBindings()
	dtui.content.container.Select(dtui.content.focusedLine.LineNum())

	if err := dtui.ui.Run(); err != nil {
		log.Fatal(err)
	}

	dtui.keyBindings()

	return dtui
}

func (dtui *DiffTUI) keyBindings() {
	dtui.ui.SetKeybinding("Up", func() { dtui.content.onScroll(0, -1) })
	dtui.ui.SetKeybinding("Down", func() { dtui.content.onScroll(0, 1) })
	dtui.ui.SetKeybinding("Left", func() { dtui.content.root.Scroll(-1, 0) })
	dtui.ui.SetKeybinding("Right", func() { dtui.content.root.Scroll(1, 0) })

	// Toggle autoscroll
	dtui.ui.SetKeybinding("s", func() {
		dtui.content.autoscroll = !dtui.content.autoscroll
		dtui.refreshStatus()
	})

	// Jump to top
	dtui.ui.SetKeybinding("u", func() {
		dtui.content.root.ScrollToTop()
		dtui.content.container.Select(dtui.content.topLine.LineNum())
		dtui.refreshStatus()
	})

	// Jump to bottom
	dtui.ui.SetKeybinding("b", func() {
		dtui.content.root.ScrollToBottom()
		dtui.content.container.Select(dtui.content.bottomLine.LineNum())
		dtui.refreshStatus()
	})
}

// updateStatus based on provided line
func (dtui *DiffTUI) updateStatus(nextSelected *diff.Line) {
	dtui.statusLabel.SetText(fmt.Sprintf(
		"Line number %d out of %d [%v%%]\nLines to end: %v\nautoscroll: %v",
		nextSelected.LineNum(),
		dtui.content.bottomLine.LineNum(),
		dtui.content.computeLinePositionInSpace(nextSelected),
		dtui.content.bottomLine.LineNum()-nextSelected.LineNum(),
		dtui.content.autoscroll))
}

// refreshStatus refresh status label based on focused line and current data
func (dtui *DiffTUI) refreshStatus() {
	dtui.updateStatus(dtui.content.focusedLine)
}
func (dtui *DiffTUI) shutdownHooks() *DiffTUI {
	dtui.ui.SetKeybinding("Esc", dtui.hardQuit)
	dtui.ui.SetKeybinding("q", dtui.hardQuit)
	return dtui
}

func (dtui *DiffTUI) validate() *DiffTUI {
	if dtui.initError != nil {
		log.Fatal(dtui.initError)
	}

	return dtui
}

func (dtui *DiffTUI) setTheme() *DiffTUI {
	t := DiffTheme()
	dtui.ui.SetTheme(t)

	return dtui
}

func (dtui *DiffTUI) hardQuit() {
	dtui.ui.Quit()
	os.Exit(0)
}
