package console

import (
	"github.com/marcusolsson/tui-go"
)

type PromptScreen struct {
	msgLabel         *tui.Label
	root             *tui.Box
	buttonsContainer *tui.Box
	//buttons          []*tui.Button
}

func acceptAction(diffTUI *DiffTUI) func(b *tui.Button) {
	return func(db *tui.Button) { // Start diff view on confirm
		diffTUI.ui.SetWidget(diffTUI.content.root)
	}
}

func declineAction(diffTUI *DiffTUI) func(b *tui.Button) {
	return func(db *tui.Button) { // Start diff view on confirm
		diffTUI.ui.SetWidget(tui.NewLabel("ok whatever"))
	}
}

// NewPrompt
func NewPrompt(onAccept, onDecline func(b *tui.Button)) *PromptScreen {
	msgLabel := tui.NewLabel("Got a new file, show?")

	confirm := tui.NewButton("[Yes]")
	confirm.OnActivated(onAccept)
	//confirm.SetFocused(true)

	decline := tui.NewButton("[No]")
	decline.OnActivated(onDecline)

	tui.DefaultFocusChain.Set(confirm, decline)
	buttons := tui.NewHBox(tui.NewPadder(1, 0, confirm), tui.NewPadder(1, 0, decline), tui.NewSpacer())
	buttons.SetSizePolicy(tui.Minimum, tui.Minimum)
	root := tui.NewHBox(msgLabel, buttons, tui.NewSpacer())

	promptScreen := &PromptScreen{msgLabel, root, buttons}
	promptScreen.root.SetBorder(true)
	promptScreen.root.SetSizePolicy(tui.Minimum, tui.Minimum)
	return promptScreen
}

// NewPromptDefaultAction ..
func NewPromptDefaultAction(dtui *DiffTUI) *PromptScreen {
	return NewPrompt(acceptAction(dtui), declineAction(dtui))
}

// NewPrompt
func NewPromptWaitOrAbort(onAccept func(b *tui.Button)) *PromptScreen {
	msgLabel := tui.NewLabel("Waiting for more files...")

	confirm := tui.NewButton("[Abort]")
	confirm.OnActivated(onAccept)
	confirm.SetFocused(true)
	//tui.DefaultFocusChain.Set(confirm)

	buttons := tui.NewHBox(tui.NewPadder(1, 0, confirm), tui.NewSpacer())
	buttons.SetSizePolicy(tui.Minimum, tui.Minimum)
	root := tui.NewHBox(msgLabel, buttons, tui.NewSpacer())

	promptScreen := &PromptScreen{msgLabel: msgLabel, root: root, buttonsContainer: buttons}
	promptScreen.root.SetBorder(true)
	promptScreen.root.SetSizePolicy(tui.Minimum, tui.Minimum)
	return promptScreen
}
