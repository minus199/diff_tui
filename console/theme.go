package console

import (
	"github.com/marcusolsson/tui-go"
)

type StyledBox struct {
	Style string
	*tui.Box
}

func (s *StyledBox) Draw(p *tui.Painter) {
	p.WithStyle(s.Style, func(p *tui.Painter) {
		s.Box.Draw(p)
	})
}

func DiffTheme() *tui.Theme {
	t := tui.NewTheme()
	//normal := tui.Style{Bg: tui.ColorWhite, Fg: tui.ColorBlack}
	//t.SetStyle("normal", normal)

	t.SetStyle("button.focused", tui.Style{Reverse: tui.DecorationOn})

	t.SetStyle("label.add", tui.Style{Bg: tui.ColorDefault, Fg: tui.ColorGreen, Bold: tui.DecorationOn})
	t.SetStyle("label.highlight-add", tui.Style{Bg: tui.ColorGreen})

	t.SetStyle("label.delete", tui.Style{Bg: tui.ColorDefault, Fg: tui.ColorRed, Underline:tui.DecorationOn})
	t.SetStyle("label.highlight-delete", tui.Style{Bg: tui.ColorRed})

	t.SetStyle("label.eq", tui.Style{Bg: tui.ColorDefault, Fg: tui.ColorBlue})
	t.SetStyle("label.highlight-eq", tui.Style{Bg: tui.ColorBlue})


	return t
}
