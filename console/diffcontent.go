package console

import (
	"github.com/marcusolsson/tui-go"
	"minus.com/diff_tui/diff"
)

// DiffContent root for diff content
type DiffContent struct {
	container *tui.Table
	root      *tui.ScrollArea

	focusedLine *diff.Line
	topLine     *diff.Line
	bottomLine  *diff.Line

	autoscroll bool
}

func NewDiffContent(topLine *diff.Line, dtui *DiffTUI) *DiffContent {
	content := &DiffContent{topLine: topLine, bottomLine: topLine.Forward(nil)}
	return content.make(dtui)
}

//make create a new content root
func (diffContent *DiffContent) make(dtui *DiffTUI) *DiffContent {
	diffContent.autoscroll = true
	diffContent.focusedLine = diffContent.topLine

	diffContent.container = tui.NewTable(0, 0)
	diffContent.container.SetFocused(true)
	diffContent.container.SetSizePolicy(tui.Maximum, tui.Minimum)
	diffContent.container.OnSelectionChanged(func(t *tui.Table) {
		diffContent.topLine.Forward(func(l *diff.Line) {
			for _, segment := range l.Segments() {
				if l.LineNum() == t.Selected() {
					diffContent.focusedLine = l
				}
				segment.SetStyle(l.LineNum() == t.Selected())
			}
		})
		dtui.refreshStatus()
	})

	diffContent.root = tui.NewScrollArea(diffContent.container)
	diffContent.root.SetSizePolicy(tui.Maximum, tui.Minimum)
	//diffContent.root.SetBorder(true)
	//root.SetColumnStretch(0, 1)
	//root.SetSizePolicy(tui.Expanding, tui.Expanding)
	//root.AppendRow(diffContent.containerScroll)

	return diffContent
}

// NewLine add a new diff line to main content
func (diffContent *DiffContent) appendDiffLine(line *diff.Line) {
	activeLine := tui.NewHBox()
	activeLine.SetSizePolicy(tui.Expanding, tui.Minimum)
	for _, segment := range line.Segments() {
		currentLineSegment := tui.NewLabel(segment.String())
		currentLineSegment.SetSizePolicy(tui.Minimum, tui.Minimum)
		segment.BindLabel(currentLineSegment).SetStyle(false)
		activeLine.Append(currentLineSegment)
	}

	activeLine.Append(tui.NewSpacer())
	diffContent.container.AppendRow(activeLine)
}

func (diffContent *DiffContent) onScroll(x float64, y float64) {
	focusedLine := diffContent.focusedLine
	if y > 0 {
		diffContent.focusedLine = diffContent.focusedLine.Next()
	}

	if y < 0 {
		diffContent.focusedLine = diffContent.focusedLine.Prev()
	}

	if diffContent.focusedLine == nil {
		diffContent.focusedLine = focusedLine
	}

	diffContent.container.Select(diffContent.focusedLine.LineNum())

	if diffContent.autoscroll &&
		diffContent.focusedLine != diffContent.bottomLine &&
		diffContent.focusedLine != diffContent.topLine {
		/*&&
	diffContent.computeLinePositionInSpace(nextSelected) > 90*/
		diffContent.root.Scroll(0, int(y)*2)
	}
}

func (diffContent *DiffContent) computeLinePositionInSpace(line *diff.Line) int {
	size := diffContent.root.Size().Y
	if size == 0 { //hacky whacky ducky
		size = 1
	}

	return (line.LineNum() * 100 / size) % 100
}
